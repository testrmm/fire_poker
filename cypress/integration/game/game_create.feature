Feature: Создание игры

  @test
  Scenario: Страница создания игры
    Given Я перехожу на главную страницу
    When Я нажимаю на ссылку с текстом "Play now"
    Then Я вижу основной заголовок страницы "Create a new game"

  @test
  Scenario: Создание игры с Fibonacci набором, Добавление структурированной стори и выбор карты - 13
    Given Я перехожу на главную страницу
    When Я нажимаю на ссылку с текстом "Play now"
    And Я заполняю текстовое поле "Your name" значением "Automation user"
    And Я заполняю текстовое поле "Game name" значением "Automation game"
    And Я заполняю текстовое поле "Game description" значением "Automation description"
    And Я заполняю большое текстовое поле "Import stories" значением "Automation story"
    And Я выбираю из списка "Choose the set of estimate cards you want to use" значение "Fibonacci: 0, ½, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 and ?"
    And Я нажимаю на кнопку с текстом "Create game"
    Then Я вижу, что выбрана вкладка "Structured"

    When Я заполняю текстовое поле "As a/an" значением "gamer1"
    And Я заполняю текстовое поле "I would like to" значением "game with u"
    And Я заполняю текстовое поле "So that" значением "Automation test"
    And Я добавляю структурированную стори
    And Я выбираю преполагаемую карту "13"
    And Я нажимаю на кнопку с текстом "Accept round"
    Then Я вижу выбранную предполагаемую карту "13"
