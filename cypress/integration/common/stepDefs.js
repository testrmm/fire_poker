import { Given, When } from "cypress-cucumber-preprocessor/steps"

Given("Я перехожу на главную страницу", function() {
    cy.visit("/")
})

When("Я нажимаю на ссылку с текстом {string}",(btnText) => {
    cy.get(`a:contains("${btnText}")`).click()
})

When("Я вижу основной заголовок страницы {string}",(header) => {
    cy.get(`h1:contains("${header}")`).should('be.visible')
})

When("Я нажимаю на кнопку с текстом {string}",(btnText) => {
    cy.get(`button:contains("${btnText}")`).click()
})
